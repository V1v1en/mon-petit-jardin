@extends('base')

@section('content')
    <div class="bg-img min-vh-100" style="background-image:url('{{ asset('images/bg4.jpg') }}');background-size:cover;">


        <div class="formContainerBig">


            <div class="container adminTable">

                <div class="row m-center">
                    <div class="col-md-12">
                        @if ($message = Session::get('success'))

                            <div class="alert alert-dismissible alert-success mt-3">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                    </div>
                </div>



                <!--:::::::::::::::::::::::::::::::::::::::::::: PARTIE ADMIN :::::::::::::::::::::::::::::::::::::::::::: -->


                <table class="table table-hover table-responsive-lg">

                    @if (Auth::user()->role === 'ADMIN')

                        <h1 class="title lead" style="font-size:max(3vw, 40px);">Gestion des annonces</h1>

                        <thead>
                            <tr class="table-active">
                                <th style="border-radius:20px 0 0 0; border:none;" scope="col">id</th>
                                <th style="border:none;" scope="col">Validation</th>
                                <th style="border:none;" scope="col">titre</th>
                                <th style="border:none;" scope="col">User_id</th>
                                <th style="border:none;" scope="col">departement</th>
                                <th style="border:none;" scope="col">ville</th>
                                <th style="border:none;" scope="col">date de l'annonce</th>
                                <th style="border-radius:0 20px 0 0; border:none;" scope="col">Action</th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach ($annonces as $annonce)

                                <tr>
                                    <th>{{ $annonce->id }}</th>
                                    <td>
                                        @if ($annonce->validation == '0')
                                            Non validé
                                        @endif
                                        @if ($annonce->validation == '1')
                                            Validé
                                        @endif
                                    </td>
                                    <td>{{ $annonce->title }}</td>
                                    <td>{{ $annonce->user_id }}</td>

                                    <td>{{ $annonce->department }}</td>
                                    <td>{{ $annonce->city }}</td>
                                    <td>{{ $annonce->date_formatted() }}</td>
                                    <td class='d-flex'>


                                        <a href="{{ route('annonce', $annonce->id) }}"
                                            class="btn btn-outline-info mx-auto">Voir</a>

                                        <button type="button" class="btn btn-outline-danger mx-auto"
                                            onclick="document.getElementById('modalDelete-{{ $annonce->id }}').style.display='block'"
                                            ;>supprimer</button>
                                        <form action="{{ route('annonces.delete', $annonce->id) }}" method="POST">
                                            @csrf
                                            @method("DELETE")
                                            <div class="modal" id="modalDelete-{{ $annonce->id }}">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Etes vous sur de vouloir supprimer votre
                                                                annonce ?</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"
                                                                onclick="document.getElementById('modalDelete-{{ $annonce->id }}').style.display='none'"
                                                                ;>
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>La suppression d'une annonce est définitive !</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">OK</button>
                                                            <button type="button" class="btn btn-secondary"
                                                                onclick="document.getElementById('modalDelete-{{ $annonce->id }}').style.display='none'"
                                                                ;>Annuler</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </form>


                                        <form method="post" action="{{ route('annonces.update', $annonce->id) }}">
                                            <button type="submit" class="btn btn-outline-success mx-auto">valider</button>
                                            @method("PUT")

                                            @csrf
                                            <input type="hidden" name="photo" value="{{ $annonce->photo }}">
                                            <input type="hidden" name="validation" value="1">
                                            <input type="hidden" name="title" value="{{ $annonce->title }}">
                                            <input type="hidden" name="description" value="{{ $annonce->description }}">
                                            <input type="hidden" name="department" value="{{ $annonce->department }}">
                                            <input type="hidden" name="city" value="{{ $annonce->city }}">
                                        </form>


                                    </td>
                            @endforeach

                            </tr>
                        </tbody>
                </table>


                @endif


                <!-- ::::::::::::::::::::::::::::::: PARTIE USER ::::::::::::::::::::::::::::::::::::::::::::::::  -->


                @if (Auth::user()->role !== 'ADMIN')


                    <h1 class="title lead" style="font-size:max(3vw, 40px);">Mes annonces</h1>
                    <thead>
                        <tr class="table-active" style="border-radius: 20px 20px 0 0;">
                            <th style="border-radius:20px 0 0 0; border:none;" scope="col">Photo</th>
                            <th style="border:none;" scope="col">Validation</th>
                            <th style="border:none;" scope="col">titre</th>
                            <th style="border:none;" scope="col">departement</th>
                            <th style="border:none;" scope="col">ville</th>
                            <th style="border:none;" scope="col">date de l'annonce</th>
                            <th style="border-radius:0 20px 0 0; border:none;" scope="col"></th>
                        </tr>
                    </thead>
                    @foreach ($annonces as $annonce)

                        @if (Auth::user()->id == $annonce->user_id)
                            <tbody class='tableResponsive'>
                                <tr>
                                    <td style="width:10%;"><img style="width:90%;"
                                            src="{{ asset("images/{$annonce->photo}") }}">
                                    </td>
                                    <td>
                                        @if ($annonce->validation == '0')
                                            Non validé
                                        @endif
                                        @if ($annonce->validation == '1')
                                            Validé
                                        @endif
                                    </td>
                                    <td>{{ $annonce->title }}</td>
                                    <td>{{ $annonce->department }}</td>
                                    <td>{{ $annonce->city }}</td>
                                    <td>{{ $annonce->date_formatted() }}</td>
                                    <td class='d-flex'>

                                        <a href="{{ route('annonces.edit', $annonce->id) }}"
                                            class="btn btn-outline-info mx-auto">Editer</a>

                                        <a href="{{ route('annonce', $annonce->id) }}"
                                            class="btn btn-outline-info mx-auto">Voir</a>

                                        <button type="button" class="btn btn-outline-danger mx-auto"
                                            onclick="document.getElementById('modalDelete-{{ $annonce->id }}').style.display='block'"
                                            ;>supprimer</button>
                                        <form action="{{ route('annonces.delete.user', $annonce->id) }}" method="POST">
                                            @csrf
                                            @method("DELETE")
                                            <div class="modal" id="modalDelete-{{ $annonce->id }}">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Etes vous sur de vouloir supprimer votre
                                                                annonce ?</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"
                                                                onclick="document.getElementById('modalDelete-{{ $annonce->id }}').style.display='none'"
                                                                ;>
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>La suppression d'une annonce est définitive !</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">OK</button>
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal"
                                                                onclick="document.getElementById('modalDelete-{{ $annonce->id }}').style.display='none'"
                                                                ;>Annuler</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>

                        @endif

                    @endforeach

                @endif

                </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection
