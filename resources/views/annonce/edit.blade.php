@extends('base')

@section('content')

<div class="container mb-5">

    <h1 class="title lead" style="font-size:max(3vw, 40px);">Editez votre petite annonce</h1>

    <div class="col-8  mx-auto">
        <div class="col-md-2 mt-1 mb-3 mx-auto">
            <img src='{{ asset("images/{$annonce->photo}") }}' style="width:90%;" alt="photo">
        </div>
    </div>

    <form method="post" action="{{ route('annonces.update', $annonce->id) }}">

        @method("PUT")

        @csrf

        <div class="col-8 mb-5 mx-auto">

            <input type="hidden" name="validation" value="0">
            <input type="hidden" name="photo" value="{{ $annonce->photo }}">

            <div class="form-group  mb-4">
                <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" placeholder="titre de votre annonce" value="{{ $annonce->title }}" required />
                @error('title')
                <span class="invalid-feedback" role="alert"></span>
                <strong>{{ $message }}</strong>
                @enderror
            </div>

            <div class="form-group mb-4">
                <textarea name="description" class="form-control w-100 @error('description') is-invalid @enderror" placeholder="Texte de votre annonce" required>{{ $annonce->description }}</textarea>
                @error('description')
                <span class="invalid-feedback" role="alert"></span>
                <strong>{{ $message }}</strong>
                @enderror
            </div>

            <div class="form-group @error('departement') is-invalid @enderror">
                <select class=" custom-select" id="departement"  name="department" required>
                <option selected="" value="{{ $annonce->department }}">{{ $annonce->department }}</option>
                <option>Ain</option>
                <option>Aisne </option>
                <option>Allier </option>
                <option>Alpes de Haute Provence </option>
                <option>Hautes Alpes </option>
                <option>Alpes Maritimes </option>
                <option>Ardèche </option>
                <option>Ardennes </option>
                <option>Ariège </option>
                <option>Aube </option>
                <option>Aude </option>
                <option>Aveyron </option>
                <option>Bouches du Rhône </option>
                <option>Calvados </option>
                <option>Cantal </option>
                <option>Charente </option>
                <option>Charente Maritime </option>
                <option>Cher </option>
                <option>Corrèze </option>
                <option>Corse du Sud </option>
                <option>Haute-Corse </option>
                <option>Côte d'Or </option>
                <option>Côtes d'Armor </option>
                <option>Creuse </option>
                <option>Dordogne </option>
                <option>Doubs </option>
                <option>Drôme </option>
                <option>Eure </option>
                <option>Eure et Loir </option>
                <option>Finistère </option>
                <option>Gard </option>
                <option>Haute Garonne </option>
                <option>Gers </option>
                <option>Gironde </option>
                <option>Hérault </option>
                <option>Ille et Vilaine </option>
                <option>Indre </option>
                <option>Indre et Loire </option>
                <option>Isère </option>
                <option>Jura </option>
                <option>Landes </option>
                <option>Loir et Cher </option>
                <option>Loire </option>
                <option>Haute Loire </option>
                <option>Loire Atlantique </option>
                <option>Loiret </option>
                <option>Lot </option>
                <option>Lot et Garonne </option>
                <option>Lozère </option>
                <option>Maine et Loire </option>
                <option>Manche </option>
                <option>Marne </option>
                <option>Haute Marne </option>
                <option>Mayenne </option>
                <option>Meurthe et Moselle </option>
                <option>Meuse </option>
                <option>Morbihan </option>
                <option>Moselle </option>
                <option>Nièvre </option>
                <option>Nord </option>
                <option>Oise </option>
                <option>Orne </option>
                <option>Pas de Calais </option>
                <option>Puy de Dôme </option>
                <option>Pyrénées Atlantiques </option>
                <option>Hautes Pyrénées </option>
                <option>Pyrénées Orientales </option>
                <option>Bas Rhin </option>
                <option>Haut Rhin </option>
                <option>Rhône </option>
                <option>Haute Saône </option>
                <option>Saône et Loire </option>
                <option>Sarthe </option>
                <option>Savoie </option>
                <option>Haute Savoie </option>
                <option>Paris </option>
                <option>Seine Maritime </option>
                <option>Seine et Marne </option>
                <option>Yvelines </option>
                <option>Deux Sèvres </option>
                <option>Somme </option>
                <option>Tarn </option>
                <option>Tarn et Garonne </option>
                <option>Var </option>
                <option>Vaucluse </option>
                <option>Vendée </option>
                <option>Vienne </option>
                <option>Haute Vienne </option>
                <option>Vosges </option>
                <option>Yonne </option>
                <option>Territoire de Belfort </option>
                <option>Essonne </option>
                <option>Hauts de Seine </option>
                <option>Seine Saint Denis </option>
                <option>Val de Marne </option>
                <option>Val d'Oise </option>
                <option">Guadeloupe </option>
                <option">Martinique </option>
                <option">Guyane </option>
                <option">Réunion </option>
                <option">Saint Pierre et Miquelon </option>
                <option">Mayotte </option>
                </select>
            </div>

            <div class="form-group mb-4">
                <input type="text" name="city" class="form-control @error('city') is-invalid @enderror" placeholder="ville" value="{{ $annonce->city }}" required />
                @error('city')
                <span class="invalid-feedback" role="alert"></span>
                <strong>{{ $message }}</strong>
                @enderror
            </div>


            <div class=" d-flex justify-content-center mb-5">
                <button type="submit" class="btn btn btn-success">Modifier</button>
            </div>

        </div>

    </form>


</div>


@endsection
