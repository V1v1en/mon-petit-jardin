@extends('base')

@section('content')
<div class="bg-img min-vh-100" style="background-image:url('{{ asset('images/bg4.jpg') }}');background-size:cover;">



    <div class="container formContainerBig">
        <div class="adminTable">
            <h1 class="title lead" style="font-size:max(3vw, 40px);">Gestion des utilisateurs</h1>
            <div class="row m-center">
                <div class="col-md-12">

                    @if ($message = Session::get('success'))

                    <div class="alert alert-dismissible alert-success mt-3">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>{{ $message }}</strong>
                    </div>

                    @endif

                </div>
            </div>

            <table class="table table-hover table-responsive-lg">

                <thead>
                    <tr class="table-active">
                        <th style="border-radius:20px 0 0 0; border:none;" scope="col" scope="col">id</th>
                        <th style="border:none;" scope="col">pseudo</th>
                        <th style="border:none;" scope="col">email</th>
                        <th style="border:none;" scope="col">enregistré le</th>
                        <th style="border-radius:0 20px 0 0; border:none;" scope="col" scope="col"></th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($users as $user)

                    <tr>
                        <th>{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td class='d-flex'>

                            <button type="button" class="btn btn-outline-danger mx-auto" onclick="document.getElementById('modalDelete-{{$user->id}}').style.display='block'" ;>supprimer</button>

                            <form action="{{ route('users.delete', $user->id) }}" method="POST">
                                @csrf
                                @method("DELETE")
                                <div class="modal" id="modalDelete-{{$user->id}}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Etes vous sur de vouloir supprimer votre annonce ?</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="document.getElementById('modalDelete-{{$user->id}}').style.display='none'" ;>
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>La suppression d'une annonce est définitive !</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">OK</button>
                                                <button type="button" class="btn btn-secondary" onclick="document.getElementById('modalDelete-{{$user->id}}').style.display='none'" ;>Annuler</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </td>

                        @endforeach

                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
