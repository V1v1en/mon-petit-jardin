@extends('base')

@section('content')



<div class="container mb-5" style="background-color : #C5DBD8">

    <h1 class="title lead" style="font-size:max(3vw, 40px);">Annonces</h1>


    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="form-group">
                    <select id="departmentSelected" class="custom-select">
                        <option>Ain</option>
                        <option>Aisne </option>
                        <option>Allier </option>
                        <option>Alpes de Haute Provence </option>
                        <option>Hautes Alpes </option>
                        <option>Alpes Maritimes </option>
                        <option>Ardèche </option>
                        <option>Ardennes </option>
                        <option>Ariège </option>
                        <option>Aube </option>
                        <option>Aude </option>
                        <option>Aveyron </option>
                        <option>Bouches du Rhône </option>
                        <option>Calvados </option>
                        <option>Cantal </option>
                        <option>Charente </option>
                        <option>Charente Maritime </option>
                        <option>Cher </option>
                        <option>Corrèze </option>
                        <option>Corse du Sud </option>
                        <option>Haute-Corse </option>
                        <option>Côte d'Or </option>
                        <option>Côtes d'Armor </option>
                        <option>Creuse </option>
                        <option>Dordogne </option>
                        <option>Doubs </option>
                        <option>Drôme </option>
                        <option>Eure </option>
                        <option>Eure et Loir </option>
                        <option>Finistère </option>
                        <option>Gard </option>
                        <option>Haute Garonne </option>
                        <option>Gers </option>
                        <option>Gironde </option>
                        <option>Hérault </option>
                        <option>Ille et Vilaine </option>
                        <option>Indre </option>
                        <option>Indre et Loire </option>
                        <option>Isère </option>
                        <option>Jura </option>
                        <option>Landes </option>
                        <option>Loir et Cher </option>
                        <option>Loire </option>
                        <option>Haute Loire </option>
                        <option>Loire Atlantique </option>
                        <option>Loiret </option>
                        <option>Lot </option>
                        <option>Lot et Garonne </option>
                        <option>Lozère </option>
                        <option>Maine et Loire </option>
                        <option>Manche </option>
                        <option>Marne </option>
                        <option>Haute Marne </option>
                        <option>Mayenne </option>
                        <option>Meurthe et Moselle </option>
                        <option>Meuse </option>
                        <option>Morbihan </option>
                        <option>Moselle </option>
                        <option>Nièvre </option>
                        <option>Nord </option>
                        <option>Oise </option>
                        <option>Orne </option>
                        <option>Pas de Calais </option>
                        <option>Puy de Dôme </option>
                        <option>Pyrénées Atlantiques </option>
                        <option>Hautes Pyrénées </option>
                        <option>Pyrénées Orientales </option>
                        <option>Bas Rhin </option>
                        <option>Haut Rhin </option>
                        <option>Rhône </option>
                        <option>Haute Saône </option>
                        <option>Saône et Loire </option>
                        <option>Sarthe </option>
                        <option>Savoie </option>
                        <option>Haute Savoie </option>
                        <option>Paris </option>
                        <option>Seine Maritime </option>
                        <option>Seine et Marne </option>
                        <option>Yvelines </option>
                        <option>Deux Sèvres </option>
                        <option>Somme </option>
                        <option>Tarn </option>
                        <option>Tarn et Garonne </option>
                        <option>Var </option>
                        <option>Vaucluse </option>
                        <option>Vendée </option>
                        <option>Vienne </option>
                        <option>Haute Vienne </option>
                        <option>Vosges </option>
                        <option>Yonne </option>
                        <option>Territoire de Belfort </option>
                        <option>Essonne </option>
                        <option>Hauts de Seine </option>
                        <option>Seine Saint Denis </option>
                        <option>Val de Marne </option>
                        <option>Val d'Oise </option>
                        <option">Guadeloupe </option>
                            <option">Martinique </option>
                                <option">Guyane </option>
                                    <option">Réunion </option>
                                        <option">Saint Pierre et Miquelon </option>
                                            <option">Mayotte </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="annonces row justify-content-center">



        @foreach ($annonces as $annonce)

        @if($annonce->validation == '1')
        <div class="col-md-6 col-lg-4 ">
            <div class="card my-4 mx-2">
                <img class="card-img-top" style="width: 100%; height: 40vh; object-fit: cover;" src='{{ asset("images/{$annonce->photo}") }}' alt="Card image cap">
                <h4 class="card-title text-center" style="height:4rem;">{{ $annonce->title }}</h4>
                <p class="card-text text-center" style="height:2rem;">{{ $annonce->department }} - {{ $annonce->city }}</p>
                <div class="text-center">
                    <a href="{{ route('annonce', $annonce->id) }}" class="btn btn-outline-success mx-auto mb-2" style="max-width:10rem;">Voir</a>
                </div>
                <p class="card-title text-center lead" style="height:4rem;">Proposé par : @ {{ $annonce->user->name }}</p>
            </div>
        </div>

        @endif


        @endforeach

    </div>

    <!-- Pagination -->

    <div class="d-flex justify-content-center mt-3 mb-5">
        {{ $annonces->links()}}
    </div>
</div>

@endsection
