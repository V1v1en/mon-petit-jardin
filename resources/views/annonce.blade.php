@extends('base')

@section('content')


<div class="jumbotron mb-5" style="background-color : #C5DBD8">

    <div class="annonces row justify-content-center">

        <div class="col-md-6">

            <div class="card annonceCard my-3 mx-2 p-2">

                <img class="card-img-top mb-2" style="width: 100%; height: 40vw; object-fit: cover;" src="{{ asset("images/{$annonce->photo}") }}" alt="Card image cap">

                <h3 class="card-title text-center mt-3" style="height:4rem;">{{ $annonce->title }}</h3>
                <p class="card-title text-center" style="height:4rem;">Proposé par : <strong>{{ $annonce->user->name}}</strong> </p>
                <p class="card-text text-center" style="height:3rem;">{{ $annonce->department }} - {{ $annonce->city }}</p>
                <p class="card-text text-center mb-5" style="height:2rem;">{{ $annonce->description }}</p>
                <div class="container">
                    <form action="mailto:{{$annonce->user->email}}" method=GET>
                        <input name=subject class="" type=hidden value="Mon&nbsp;petit&nbsp;jardin&nbsp;:&nbsp;une&nbsp;message&nbsp;pour&nbsp;vous&nbsp;!">
                        <h5 class="text-center mb-4" style="color:#;">Envoyer un message</h5>
                        <p class="text-center mb-2">
                            <textarea name=body class="form-control mb-4 lead" rows="3">

                        </textarea>
                            <br>
                            <input type=submit class="btn btn-outline-success" style="max-width:10rem;" value="envoyer">
                        </p>

                    </form>
                    <form>
                        <input type="button" class="btn btn-outline-success" value="<" onclick="history.go(-1)">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
