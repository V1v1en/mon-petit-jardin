@extends('base')


@section('content')

<div class="bg-img-fluid pb-5" style="background-image:url('{{ asset('images/bg6.jpg') }}');background-size:auto; min-height:100vh;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="text-center">
                    <img src="{{ asset('images/titre.png') }}" class="titre img-fluid"  style="min-width:80%;" alt="titre du site mon petit jardin" />
                </div>
            </div>
            <div class="col-md-3 d-flex justify-content-center">

                    <p class="mt-5">
                    <div class="card border-light  d-flex mt-5" style="max-width: 30rem;">
                        <div class="card-body">
                            <h4 class="card-title">Hello 👋 les jardiniers !</h4>
                            <p class="card-text">Bienvenue sur un site de petites annonces pas comme les autres. Le but ici n'est pas de s'enrichir, mais de partager, d'échanger ou éventuellement de vendre à un cout raisonnable des plantes et arbustes.</p>
                            <p class="card-text">Tout le monde peut consulter les annonces, toutefois pour en crééer une vous devez vous connecter, idem pour contacter un acheteur.
                            <p>
                            <footer class="blockquote-footer">L'équipe de <cite title="Source Title">Mon petit Jardin</cite></footer>
                        </div>
                    </div>
                    </p>
                </div>

        </div>
    </div>
</div>

@endsection
