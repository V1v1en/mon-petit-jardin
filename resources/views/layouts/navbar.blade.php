<nav class="navbar navbar-expand-md navbar-light bg-light">
    <a class="navbar-brand" href="#"></a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon text-center"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor03">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('home')}}">Home
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('annonces')}}">Annonces</a>
            </li>

            @if(Auth::user())
            @if (Auth::user()->role !== 'ADMIN')
            <li class="nav-item">
                <a class="nav-link" href="{{route('create')}}">créer une annonce</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('mesannonces')}}">Mes annonces</a>
            </li>
            @endif

            @endif
        </ul>
        <ul class="navbar-nav ml-auto">
            <!-- si l'utilisateur est identifié -->
            @if (Auth::user())

            @if (Auth::user()->role == 'ADMIN')
            <li class="nav-item mb-2">

                <a href="{{route('annonces.index')}}">
                    <button class="btn">ADMIN ANNONCES</button>
                </a>

                <a href="{{route('users.index')}}">
                    <button class="btn">ADMIN USERS</button>
                </a>
            </li>
            @endif

            @if (Auth::user()->role !== 'ADMIN')
            <li class="nav-item mb-2">
                <a href="{{route('annonces.index')}}">
                    <button class="btn">{{Auth::user()->name}}</button>
                </a>
            </li>
            @endif
            <!-- creation d'un formulaire pour la deconnexion qui n'accepte que la méthode POST -->
            <li class="nav-item mb-0">
                <form method="POST" action="{{route('logout')}}">
                    @csrf
                    <button type="submit" class="btn">Déconnexion</button>
                </form>
            </li>
            <!-- si l'utilisateur n'est pas identifié -->
            @else

            <li class="nav-item mb-2">
                <a href='{{route("login")}}'>
                    <button class="btn">Connexion</button>
                </a>
            </li>
            @endif
        </ul>
    </div>
</nav>
