<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Foundation\Auth\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Annonce extends Model
{
    use HasFactory;

    protected $fillable = [
      'user_id', 'validation', 'description', 'photo', 'department', 'city', 'title',

    ];

    // creation d'une fonction qui permet de retourner le champs date formaté de manière plus simple que le created_at

    public function date_formatted(){

    return date_format($this->created_at , 'd/m/y');

    }

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id');
    }
}

class Post extends Model
{
    use Searchable;
}
