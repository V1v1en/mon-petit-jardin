<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();

        // si l'utilisateur n'est pas authentifié il est redirigé vers la page de connexion

        if (!$user) {
            return redirect()->route('login');
        }

        // si l'utilisateur est authentifié mais qu'il n'est pas administrateur il est aussi redirigé vers la page de connexion

        if ($user->role !== User::ADMIN_ROLE) {
            return redirect()->route('login');
        }
        // sinon il est donc admin et peux accéder aux ressources

        return $next($request);
    }
}
