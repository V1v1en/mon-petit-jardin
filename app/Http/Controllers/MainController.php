<?php

namespace App\Http\Controllers;

use App\Models\Annonce;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class MainController extends Controller
{
    public function home()
    {
        return view('home');
    }


// Permet la pagination de la page index avec toute les annonces

    public function index()
    {
        $annonces = Annonce::paginate(6);

        return view('annonces', [
            'annonces' => $annonces
        ]);
    }

// retourne la vue d'une seule annonce

    public function show(Annonce $annonce)    {

        return view('annonce', [
            'annonce' => $annonce

        ]);
    }

}
