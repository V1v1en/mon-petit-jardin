<?php

namespace App\Http\Controllers;

use view;
use App\Models\User;
use App\Models\Annonce;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AnnonceRequest;

class AnnonceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $annonces = Annonce::paginate(30);
        // $annonces = Annonce::with('User:id,name')->get();

        // dd($annonces);
        return view('annonce.index', ['annonces' => $annonces]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\AnnonceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnonceRequest $request)
    {
        $validated = $request->validated();

        //  dd(Auth::user()->name);

        Annonce::create([
            'user_id' => Auth::user()->id,
            'title' => $request->input('title'),
            'validation' => $request->input('validation'),
            'description' => $request->input('description'),
            'photo' => $request->input('photo'),
            'department' => $request->input('department'),
            'city' => $request->input('city'),
            'validation' => $request->input('validation'),


        ]);

        return redirect()->route('mesannonces')->with('success', "L'annonce  bien été créée ✌ Elle sera mise en ligne après modération 😉");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Annonce $annonce)
    {
        return view("annonce.edit", ['annonce' => $annonce]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnnonceRequest $request, Annonce $annonce)
    {
        $annonce->title = $request->input('title');
        $annonce->description = $request->input('description');
        $annonce->photo = $request->input('photo');
        $annonce->department = $request->input('department');
        $annonce->city = $request->input('city');
        $annonce->validation = $request->input('validation');

        $annonce->save();

        return redirect()->route('mesannonces')->with('success', "L'annonce a bien été modifiée ✌");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function delete(Annonce $annonce)
    {
        $annonce->delete();

        if (Auth::user()->role === 'ADMIN'){
            return redirect()->route('annonces.index')->with('success', "l'annonce a bien été supprimée 🐱‍👤");

        }else{

            return redirect()->route('mesannonces')->with('success', "l'annonce a bien été supprimée 🐱‍👤");
        }

    }

}

