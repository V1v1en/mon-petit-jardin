<?php

namespace App\Observers;

use App\Models\Annonce;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Str;



class AnnonceObserver
{
    /**
     * Handle the Annonce "created" event.
     *
     * @param  \App\Models\Annonce  $annonce
     * @return void
     */
    public function created(Annonce $annonce)
    {
        // creation de la fonction avec la fonction helpers slug

        $annonce->slug =Str::slug($annonce->title, '-');
        $annonce->save();
    }

    /**
     * Handle the Annonce "updated" event.
     *
     * @param  \App\Models\Annonce  $annonce
     * @return void
     */
    public function updated(Annonce $annonce)
    {
        $annonce->slug =Str::slug($annonce->title, '-');
        $annonce->saveQuietly();
    }

    /**
     * Handle the Annonce "deleted" event.
     *
     * @param  \App\Models\Annonce  $annonce
     * @return void
     */
    public function deleted(Annonce $annonce)
    {
        //
    }

    /**
     * Handle the Annonce "restored" event.
     *
     * @param  \App\Models\Annonce  $annonce
     * @return void
     */
    public function restored(Annonce $annonce)
    {
        //
    }

    /**
     * Handle the Annonce "force deleted" event.
     *
     * @param  \App\Models\Annonce  $annonce
     * @return void
     */
    public function forceDeleted(Annonce $annonce)
    {
        //
    }
}
