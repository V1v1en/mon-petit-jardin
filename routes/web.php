<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AnnonceController;
use App\Http\Controllers\ImageUploadController;



// !! ^^  Bien penser à importer la class à chaque fois (ctrl + Alt + I) ^^

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route corespondant au controller home

Route::get('/', [ MainController::class, 'home' ])->name('home');

// Route pour acceder aux annonces

Route::get('/annonces' , [ MainController::class, 'index'])->name('annonces');

//Route pour accéder a une annonce sans passer par le slug (méthode magique de Laravel)

Route::get('/annonces/{annonce}/' , [ MainController::class, 'show'])->name('annonce');

// permet de relier toutes les routes à l'athentification

Auth::routes();


//Vues ADMIN validation annonces et modération

Route::get('/admin/annonces' , [ AnnonceController::class, 'index'])->middleware('admin')->name('annonces.index');

Route::delete('/admin/annonces/{annonce:id}/delete' , [ AnnonceController::class, 'delete'])->middleware('admin')->name('annonces.delete');

Route::get('/admin/users' , [ UserController::class, 'index'])->middleware('admin')->name('users.index');

Route::delete('/admin/users/{user:id}/delete' , [ UserController::class, 'delete'])->middleware('admin')->name('users.delete');



//Vues user pour création modification suppression et visualisation des annonces

Route::get('/mesannonces' , [ AnnonceController::class, 'index'])->middleware('auth')->name('mesannonces');

Route::post('/create/store/' , [ AnnonceController::class, 'store'])->middleware('auth')->name('annonces.store');

Route::get('/create' , [ AnnonceController::class, 'create'])->middleware('auth')->name('create');

Route::get('/annonce/{annonce}/edit' , [ AnnonceController::class, 'edit'])->middleware('auth')->name('annonces.edit');

Route::put('/admin/annonce/{annonce}/update' , [ AnnonceController::class, 'update'])->middleware('auth')->name('annonces.update');

Route::delete('/annonces/{annonce:id}/delete' , [ AnnonceController::class, 'delete'])->middleware('auth')->name('annonces.delete.user');


// Gestion des images

Route::get('image-upload', [ ImageUploadController::class, 'imageUpload' ])->name('image.upload');

Route::post('image-upload', [ ImageUploadController::class, 'imageUploadPost' ])->name('image.upload.post');


