<?php

namespace Database\Seeders;

use Faker\Factory;
use App\Models\Annonce;
use Illuminate\Database\Seeder;

class AnnonceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // On fait appel au model Article et la méthode create qui prend un tableau de parametres avec les champs que l'on rempli un par un.

        $faker = Factory::create();

        for ($i=0; $i < 26; $i++) {
            Annonce::create([
                'title' => $faker->sentence(),
                'validation' => $faker->boolean(false),
                'description' => $faker->text($maxNBChars =200),
                'photo' => $faker->imageUrl($width = 320, $height = 240),
                'department' => $faker->state(),
                'city' => $faker->city(),
            ]);
        }

    }
}
