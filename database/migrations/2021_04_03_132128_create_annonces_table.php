<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnoncesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annonces', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table
            ->boolean('validation')
            ->default(false);

            $table
            ->unsignedBigInteger('user_id')
            ->references('id')
            ->on('users');
            $table
            ->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->string('title');
            $table->text('description');
            $table->string('photo');
            $table->string('department');
            $table->string('city');

            $table->string('slug')
            ->nullable();


        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annonces');
    }
}
